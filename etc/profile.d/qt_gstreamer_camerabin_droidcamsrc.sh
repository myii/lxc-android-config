# Tell Qt's gstcamerabin plugin which element should be used as
# camerabin's source
# (This file is part of lxc-android-config)

export QT_GSTREAMER_CAMERABIN_SRC=droidcamsrc
